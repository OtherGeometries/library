    // set stages as variables:

    let a = document.querySelector("#stage1");
    let b = document.querySelector("#stage2");
    let c = document.querySelector("#stage3");

    // vocabulary: 

        let vocabulary = {};

        // BREATHING
        vocabulary["breathing"] = {}
        vocabulary["breathing"].DOM =
            ((time,value) => { Tone.Draw.schedule(() => {
                a.style.transform=`scale(${value.args[2]})`;
                console.log('dancer 03: breathing ()');
                 }, time);

            })

        // DISSOLVING
        vocabulary["dissolving"] = {}
        vocabulary["dissolving"].DOM =
             ((time,value) => { Tone.Draw.schedule(() => {
                b.style.filter=`blur(${value.args[0]})`;
                console.log('dancer 02: dissolving ()');
                }, time);
             })

        // BALANCING
        vocabulary["balancing"] = {}
        vocabulary["balancing"].DOM =
             ((time,value) => { Tone.Draw.schedule(() => {
                c.style.transform = `skew(${value.args[0]})`; 
                console.log('dancer 01: balancing ()');
                }, time);
             })
