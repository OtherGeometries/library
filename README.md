# Library

Documentation of the Choreographies of the Circle & Other Geometries JS library.


## Description

Choreographies of the Circle & Other Geometries is research project on socio-technical protocols for collaborative audio-visual live coding and a corresponding peer-to-peer environment programmed in JavaScript.

The project reflects how language boundaries are enacted through the computing environment and within social spheres. It explores how movement, gestures, discourses, and behaviours are choreographed and communicated through the apparatuses at work and how our hybrid digital systems and transdisciplinary research practices co-construct each other. It is informed by on-going research and recollection of musical and choreographic sources and scores that reference principles of non-linear composition, non-hegemonic time and space constructs, techno-feminist understandings.

Project initiated by [Joana Chicau](https://joanachicau.com/) and [Renick Bell](https://renickbell.net/doku.php).

Link to project's website: https://geometries.xyz/


## Installation

Please refer to our [Wiki](https://geometries.xyz/wiki/doku.php?id=installation) for details on how to install this software.

## Usage

Please refer to our [Wiki](https://geometries.xyz/wiki/doku.php?id=start) for details, examples and tutorials on how to use this software.


## Support

You can ask for support through the issue tracker or by contacting the authors: [Joana Chicau](https://joanachicau.com/) and [Renick Bell](https://renickbell.net/doku.php).

## Roadmap

Future Dreams:

* ...

## Contributing

We welcome contributions and feedback on the project.

More information on how to contribute soon.

## Authors and acknowledgment

Authors: [Joana Chicau](https://joanachicau.com/) and [Renick Bell](https://renickbell.net/doku.php).

The research conducted 2021-2021 was supported by [Creative Industries Fund NL](https://www.stimuleringsfonds.nl/en) for Digital Culture.

Thank you to the support of the live coding community, among many, Akihiro Kubota, Marije Baalman, Matthew Yee-King and Olivia Jack.

## License

Copyright (c) 2019-2022 Joana Chicau and Renick Bell.

The source code for the project Choreographies of the Circle & Other Geometries is under Collective Conditions for Re-Use (CC4r) License.

Copyleft with a difference: This is a collective work, you are invited to copy, distribute, and modify it under the terms of the CC4r as found in the LICENSE file in the source code root directory at <https://gitlab.constantvzw.org/unbound/cc4r>.

The only restriction is that you provide attribution to the authors.

The project comes with ABSOLUTELY NO WARRANTY, to the extent permitted by applicable law.  See the CNPL for details.


## Project status

This project is under development. 
